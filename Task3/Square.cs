﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task3
{
    class Square
    {
        public void CreateSquare(int size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (j == 0 || j == size - 1 || i == 0 || i == size - 1)
                    {
                        Console.Write("#");
                    }
                    else { Console.Write(" "); }
                }
                Console.WriteLine(" ");
            }
        }
    }
}
