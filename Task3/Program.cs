﻿using System;
using System.Collections.Generic;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            //get user input size
            int userInputSize = Convert.ToInt32(Console.ReadLine());

            //create square
            Square square = new Square();

            square.CreateSquare(userInputSize);


        }
    }


}
